## Example Playbook
```
- hosts: server
  become: yes
  vars:
    nfs_mode: server
    nfs_exports:
      - path: "/home"
        export: " *(rw,insecure,nohide,all_squash,no_subtree_check)"
  roles:
    - nfs

- hosts: client
  become: yes
  vars:
    nfs_mode: client
    nfs_mount:
      - folder: "/data/backup1"
        remote: "/home"
        fstype: "nfs4"
        server_host: "127.0.0.1"
  roles:
    - nfs
```